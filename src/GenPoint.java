
public class GenPoint {
	
	int dim;
	double coor[];
	
	public GenPoint(double coor[]){
		dim = coor.length;
		this.coor = new double[dim];
		for(int i = 0; i < dim; i++){
			this.coor[i] = coor[i];
		}
	}
	
	public GenPoint(){
		
	}
	
	
	
	public double norm(){
		double norm = 0;
		for(int i = 0; i < dim; i++){
			norm+= coor[i]*coor[i];
		}
		norm = Math.sqrt(norm);
		return norm;
	}
	
	public GenPoint sum(GenPoint point){
		int dim = point.dim;
		double a[] = new double[dim];
		for(int i = 0; i < dim; i++){
			a[i] = this.coor[i] + point.coor[i]; 
		}
		GenPoint m = new GenPoint(a);
		return m;
		
	}
	
	public GenPoint diff(GenPoint point){
		int dim = point.dim;
		double a[] = new double[dim];
		for(int i = 0; i < dim; i++){
			a[i] = this.coor[i] - point.coor[i]; 
		}
		GenPoint m = new GenPoint(a);
		return m;
		
	}
	
	public GenPoint scalarMultiple(double x){
		double a[] = new double[dim];
		for(int i = 0; i < dim; i++){
			a[i] = coor[i]*x;
		}
		GenPoint m = new GenPoint(a);
		return m;
	}
	
}
