import java.io.IOException;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;

public class LinProgram {

	private LpSolve solver;
	private int acc;
	private int dec;

	public LinProgram(int x, int y) throws LpSolveException {
		acc = x;
		dec = y;
		solver = LpSolve.makeLp(0, acc * dec);
	}

	void initialize(double val[], double capBound) throws LpSolveException {
		int num = acc * dec + 1;
		double[] arr = new double[num];
		for (int i = 0; i < acc * dec; i++) {
			arr[i] = 0;
		}

		solver.setVerbose(0);
		boolean cap = true;
		if (cap) {
			for (int i = 0; i < dec; i++) {
				for (int j = i * acc; j < (i + 1) * acc; j++) {
					arr[j + 1] = 1;
				}
				solver.addConstraint(arr, LpSolve.LE, capBound);
				for (int j = i * acc; j < (i + 1) * acc; j++) {
					arr[j + 1] = 0;
				}
			}
		}
		for (int i = 0; i < acc; i++) {
			for (int j = 0; j < dec; j++) {
				arr[i + acc * j + 1] = 1;
			}
			solver.addConstraint(arr, LpSolve.EQ, val[i]);
			for (int j = 0; j < dec; j++) {
				arr[i + acc * j + 1] = 0;
			}
		}

	}

	void solve(double valcon[], double cmatrix[][]) throws IOException, LpSolveException {

		solver.setObjFn(valcon);
		solver.setMinim();
		solver.solve();
		double[] var = solver.getPtrVariables();
		for (int i = 0; i < dec; i++) {
			for (int j = 0; j < acc; j++) {
				cmatrix[i][j] = var[i * acc + j];
			}
		}
	}

}
