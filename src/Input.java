import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Input {

	Point map[];
	double capacities[];
	int NumberOfPoints;
	double distanceMatrix[][];
	double sumCap;
	GenPoint emPoints[];

	public Input() {
		try {
			start();
			//getDistances();
			getEmbeddedPoints();
		} catch (IOException e) {
			System.out.println("Cannot initilize input");
		}
	}

	public void start() throws IOException {
		NumberOfPoints = 0;
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream("C:\\Users\\palan\\workspace\\JsonDecode\\lonlatdataGe.txt")));
		String distance = "";
		
		try {
			String line;
			while ((line = br.readLine()) != null) {
				NumberOfPoints++;
				distance += line + " ";
			}
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			br.close();
		}
		String a[] = distance.split(" ");
		//System.out.println(m);
		map = new Point[NumberOfPoints];
		capacities = new double[NumberOfPoints];
		sumCap = 0;
		for (int i = 0; i < NumberOfPoints; i++) {
			map[i] = new Point(Double.parseDouble(a[4 * i + 1]), Double.parseDouble(a[4 * i + 2]));
			capacities[i] = Double.parseDouble(a[4 * i + 3]);
			sumCap+=capacities[i];
		}
	}
	
	public void getDistances() throws IOException{
		distanceMatrix = new double[NumberOfPoints][NumberOfPoints];
		
		BufferedReader br1 = new BufferedReader(
				new InputStreamReader(new FileInputStream("C:\\Users\\palan\\workspace\\JsonDecode\\distancedataAu.txt")));
		int indI = 0;
		int indJ = 0;
		try {
			String line;
			while ((line = br1.readLine()) != null) {
				String b[] = line.split(" ");
				distanceMatrix[indI][indJ] = Double.parseDouble(b[2]);
				//System.out.println(distanceMatrix[indI][indJ]);
				indJ++;
				if(indJ == NumberOfPoints){
					indJ = 0;
					indI++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br1.close();
		}
		
	}
	
	public void getEmbeddedPoints() throws IOException{
		ArrayList<GenPoint> list = new ArrayList<GenPoint>();
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream("C:\\Users\\palan\\workspace\\GraphEmbedding\\PointsAu.txt")));
		try {
			String line;
			while ((line = br.readLine()) != null) {
				String coor[] = line.split(" ");
				double coords[] = new double[coor.length];
				for(int i = 0; i < coor.length; i++){
					coords[i] = Double.parseDouble(coor[i]);
				}
				GenPoint tmp = new GenPoint(coords);
				list.add(tmp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		emPoints = list.toArray(new GenPoint[0]);
	}

}
