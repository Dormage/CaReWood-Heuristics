import java.io.IOException;
import java.net.URI;

import lpsolve.LpSolveException;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class NetworkManager extends WebSocketClient{
	
	public NetworkManager(URI serverURI) {
		super(serverURI);
	}

	@Override
	public void onClose(int arg0, String arg1, boolean arg2) {
		System.out.println("Closing connection...");
		
	}
	@Override
	public void onError(Exception arg0) {
		System.out.println("Network error: " + arg0);
		
	}
	@Override
	public void onMessage(String message){
		Main.networkTimeDelta = System.currentTimeMillis();
			try {
				Main.runTests(Double.parseDouble(message));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LpSolveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	@Override
	public void onOpen(ServerHandshake arg0) {
		System.out.println("Handasake with server succesfull");
		
	}

}
