import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

public class Parser {

	PrintWriter writer2;
	DecimalFormat df;
	DecimalFormat df1;
	public Parser() throws FileNotFoundException, UnsupportedEncodingException{
		writer2 = new PrintWriter("austria_parsed.txt", "UTF-8");
		df = new DecimalFormat("#.#######");
		df1 = new DecimalFormat("#.##");
	}
	
	public String euclidParseNetworked(Point acc[], Point centers[],double acccap[], double faccap[]){
		StringBuilder data = new StringBuilder();
		data.append("accumulation:");
		for(int i = 0; i< acc.length; i++){
			data.append("    acccenter " + (i+1) + "=");
			data.append(System.getProperty("line.separator"));
			data.append("        latitude: " + df.format(acc[i].lat) + ",");
			data.append(System.getProperty("line.separator"));
			data.append("        longitude: " + df.format(acc[i].lon) + ",");
			data.append(System.getProperty("line.separator"));
			data.append("        capacity: " + df1.format(acccap[i]) + ",");
			data.append(System.getProperty("line.separator"));
			data.append("        building_cost: " + 0 + ",");
			data.append(System.getProperty("line.separator"));
			data.append("        operation_cost: " + 0 );
			data.append(System.getProperty("line.separator"));
			data.append("        ;");
			data.append(System.getProperty("line.separator"));
			//data.append(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
		}
			data.append("    ;");
			data.append(System.getProperty("line.separator"));
			data.append("sorting:");
			data.append(System.getProperty("line.separator"));
			for(int i = 0; i< centers.length; i++){
				data.append("    sortcenter " + (i+1) + "=");
				data.append(System.getProperty("line.separator"));
				data.append("        latitude: " + df.format(centers[i].lat) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        longitude: " + df.format(centers[i].lon) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        capacity: " + df1.format(faccap[i]) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        building_cost: " + 3000000 + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        operation_cost: " + 200000 );
				data.append(System.getProperty("line.separator"));
				data.append("        ;");
				data.append(System.getProperty("line.separator"));
				//data.append(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//	System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			}
				data.append("    ;");
				data.append(System.getProperty("line.separator"));
			data.append("decontamination:");
			data.append(System.getProperty("line.separator"));
			for(int i = 0; i< centers.length; i++){
				data.append("    decontcenter " + (i+1) + "=");
				data.append(System.getProperty("line.separator"));
				data.append("        latitude: " + df.format(centers[i].lat) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        longitude: " + df.format(centers[i].lon) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        capacity: " + df1.format(faccap[i]) + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        building_cost: " + 3000000 + ",");
				data.append(System.getProperty("line.separator"));
				data.append("        operation_cost: " + 200000 );
				data.append(System.getProperty("line.separator"));
				data.append("        ;");
				data.append(System.getProperty("line.separator"));
				//data.append(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//	System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			}
				data.append("    ;");
				data.append(System.getProperty("line.separator"));
				data.append("distances:");
				data.append(System.getProperty("line.separator"));
				
				for(int i = 0; i<centers.length; i++){
					for(int j = 0; j<acc.length; j++){
						data.append("    from: sortcenter " + (i+1) + "," );
						data.append(System.getProperty("line.separator"));
						data.append("    to: acccenter " + (j+1) + "," );
						data.append(System.getProperty("line.separator"));
						double ldis = centers[i].diff(acc[j]).norm();
						data.append("    distance: " + (int)ldis);
						data.append(System.getProperty("line.separator"));
						data.append("        ;");
						data.append(System.getProperty("line.separator"));
					}
					for(int j = 0; j<centers.length; j++){
						data.append("    from: sortcenter " + (i+1) + "," );
						data.append(System.getProperty("line.separator"));
						data.append("    to: decontcenter " + (j+1) + "," );
						data.append(System.getProperty("line.separator"));
						double ldis = centers[i].diff(centers[j]).norm();
						data.append("    distance: " + (int)ldis);
						data.append(System.getProperty("line.separator"));
						data.append("        ;");
						data.append(System.getProperty("line.separator"));
					}
				}
					for(int i = 0; i<centers.length; i++){
						for(int j = 0; j<acc.length; j++){
							data.append("    from: decontcenter " + (i+1) + "," );
							data.append(System.getProperty("line.separator"));
							data.append("    to: acccenter " + (j+1) + "," );
							data.append(System.getProperty("line.separator"));
							double ldis = centers[i].diff(acc[j]).norm();
							data.append("    distance: " + (int)ldis);
							data.append(System.getProperty("line.separator"));
							data.append("        ;");
							data.append(System.getProperty("line.separator"));
						}
						
					}
					data.append("        ;");
					data.append(System.getProperty("line.separator"));
					
		return data.toString();
	}
	
	
	public void euclidParse(Point acc[], Point centers[],double acccap[], double faccap[]){
		writer2.println("accumulation:");
		for(int i = 0; i< acc.length; i++){
			writer2.println("    acccenter " + (i+1) + "=");
			writer2.println("        latitude: " + df.format(acc[i].lat) + ",");
			writer2.println("        longitude: " + df.format(acc[i].lon) + ",");
			writer2.println("        capacity: " + df1.format(acccap[i]) + ",");
			writer2.println("        building_cost: " + 0 + ",");
			writer2.println("        operation_cost: " + 0 );
			writer2.println("        ;");
			//writer2.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
		}
			writer2.println("    ;");
			writer2.println("sorting:");
			for(int i = 0; i< centers.length; i++){
				writer2.println("    sortcenter " + (i+1) + "=");
				writer2.println("        latitude: " + df.format(centers[i].lat) + ",");
				writer2.println("        longitude: " + df.format(centers[i].lon) + ",");
				writer2.println("        capacity: " + df1.format(faccap[i]) + ",");
				writer2.println("        building_cost: " + 3000000 + ",");
				writer2.println("        operation_cost: " + 200000 );
				writer2.println("        ;");
				//writer2.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//	System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			}
				writer2.println("    ;");
			writer2.println("decontamination:");
			for(int i = 0; i< centers.length; i++){
				writer2.println("    decontcenter " + (i+1) + "=");
				writer2.println("        latitude: " + df.format(centers[i].lat) + ",");
				writer2.println("        longitude: " + df.format(centers[i].lon) + ",");
				writer2.println("        capacity: " + df1.format(faccap[i]) + ",");
				writer2.println("        building_cost: " + 3000000 + ",");
				writer2.println("        operation_cost: " + 200000 );
				writer2.println("        ;");
				//writer2.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			//	System.out.println(df.format(centers[i].lat/1000) + " " + df.format(centers[i].lon/1000) + " " + df1.format(capacities[i]));
			}
				writer2.println("    ;");
				writer2.println("distances:");	
				
				for(int i = 0; i<centers.length; i++){
					for(int j = 0; j<acc.length; j++){
						writer2.println("    from: sortcenter " + (i+1) + "," );
						writer2.println("    to: acccenter " + (j+1) + "," );
						double ldis = centers[i].diff(acc[j]).norm();
						writer2.println("    distance: " + (int)ldis);
						writer2.println("        ;");
					}
					for(int j = 0; j<centers.length; j++){
						writer2.println("    from: sortcenter " + (i+1) + "," );
						writer2.println("    to: decontcenter " + (j+1) + "," );
						double ldis = centers[i].diff(centers[j]).norm();
						writer2.println("    distance: " + (int)ldis);
						writer2.println("        ;");
					}
				}
					for(int i = 0; i<centers.length; i++){
						for(int j = 0; j<acc.length; j++){
							writer2.println("    from: decontcenter " + (i+1) + "," );
							writer2.println("    to: acccenter " + (j+1) + "," );
							double ldis = centers[i].diff(acc[j]).norm();
							writer2.println("    distance: " + (int)ldis);
							writer2.println("        ;");
						}
						
					}
					writer2.println("        ;");
					writer2.close();
	}
	
}
