
public class Point {
	
	public double lat;
	public double lon;
	
	public Point(double lat, double lon){
		this.lat = lat;
		this.lon = lon;
		
	}
	public Point(){
		
	}
	
	public double norm(){
		double y = lat*110.574;
		double x = lon*111.32 * Math.cos(0.82);
		return Math.sqrt(x*x + y*y);
	}
	
	public Point sum (Point point){
		double x = point.lat + lat;
		double y = point.lon + lon;
		Point m = new Point(x,y);
		return m;
	}
	
	public Point diff (Point point){
		double x = point.lat - lat;
		double y = point.lon - lon;
		Point m = new Point(x,y);
		return m;
	}
	public Point scalarMultiple(double x){
		return new Point(lat*x,lon*x);
	}

}
