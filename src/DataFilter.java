import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

public class DataFilter {

	Point map[];

	double capacities[];
	int NumberOfPoints;
	double distanceMatrix[][];
	double sumCap;
	private JSONObject distanceMatrixJson;
	private JSONArray dataSetJson;

	public int[] GenerateIndices(int n) {
		int indexArr[] = new int[NumberOfPoints];
		boolean indexCheck[] = new boolean[n];
		int tmpbr = 0;
		while (tmpbr < NumberOfPoints) {
			int rnd = (int) (n * Math.random());
			if (indexCheck[rnd])
				continue;
			indexArr[tmpbr] = rnd;
			indexCheck[rnd] = true;
			tmpbr++;
		}

		return indexArr;
	}

	public void JsonSampleGenerator(int[] indexArr, int sampleIndex)
			throws FileNotFoundException, UnsupportedEncodingException {
		File file1 = new File("" + NumberOfPoints);
		file1.mkdir();
		PrintWriter lonlatRandJson = new PrintWriter(NumberOfPoints + "/dataSet" + sampleIndex + ".json", "UTF-8");
		PrintWriter distanceRandJson = new PrintWriter(NumberOfPoints + "/distanceRandJson" + sampleIndex + ".json",
				"UTF-8");

		Map<String, Integer> names = new HashMap<String, Integer>();

		map = new Point[indexArr.length];
		capacities = new double[indexArr.length];
		distanceMatrix = new double[indexArr.length][indexArr.length];

		sumCap = 0;

		JSONArray lonlatRand = new JSONArray();
		for (int i = 0; i < indexArr.length; i++) {
			JSONObject obj = dataSetJson.getJSONObject(indexArr[i]);
			String name = (String) obj.get("name");
			lonlatRand = lonlatRand.put(obj);
			names.put(name, i);
			String lat = obj.getString("la");
			String lon = obj.getString("lo");
			map[i] = new Point(Double.parseDouble(lat), Double.parseDouble(lon));
			int cap = obj.getInt("capacity");
			capacities[i] = (double) cap;
			sumCap += capacities[i];
		}

		JSONObject distanceRand = new JSONObject();
		
		for (String key : names.keySet()) {
			JSONObject obj = distanceMatrixJson.getJSONObject(key);
			JSONObject helpobj = new JSONObject();
			for (String key1 : names.keySet()) {
				int dist = obj.getInt(key1);
				distanceMatrix[names.get(key)][names.get(key1)] = dist;
				helpobj.put(key1, dist);
			}
			distanceRand.put(key, helpobj);
		}

		lonlatRandJson.print(lonlatRand);
		distanceRandJson.print(distanceRand);
		lonlatRandJson.close();
		distanceRandJson.close();

	}

	public static void ReadableSampleGenerator(int[] indexArr, JSONObject distanceobj, JSONArray lonlatarr)
			throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter lonlatRand = new PrintWriter("lonlatRand.txt", "UTF-8");
		PrintWriter distanceRand = new PrintWriter("distanceRand.txt", "UTF-8");

		Set<String> names = new HashSet<String>();

		for (int i : indexArr) {
			JSONObject obj = lonlatarr.getJSONObject(i);
			String name = obj.getString("name");
			String lat = obj.getString("la");
			String lon = obj.getString("lo");
			int cap = obj.getInt("capacity");
			names.add(name);
			lonlatRand.print(name.replace(" ", "") + " ");
			lonlatRand.print(lat + " ");
			lonlatRand.print(lon + " ");
			lonlatRand.print(cap);
			lonlatRand.println();

		}

		for (String key : names) {
			JSONObject obj = distanceobj.getJSONObject(key);
			for (String key1 : names) {
				int dist = obj.getInt(key1);
				distanceRand.println(key.replace(" ", "") + " " + key1.replace(" ", "") + " " + dist);
			}
		}

		lonlatRand.close();
		distanceRand.close();
	}

	public int ReadFiles(File dataSet, File distanceMatrix) throws IOException {

		FileInputStream fis = new FileInputStream(dataSet);
		byte[] data = new byte[(int) dataSet.length()];
		fis.read(data);
		fis.close();

		String str = new String(data, "UTF-8");

		dataSetJson = new JSONArray(str);

		FileInputStream fis1 = new FileInputStream(distanceMatrix);
		byte[] data1 = new byte[(int) distanceMatrix.length()];
		fis1.read(data1);
		fis1.close();

		String str1 = new String(data1, "UTF-8");

		distanceMatrixJson = new JSONObject(str1);

		return dataSetJson.length();

	}
}
