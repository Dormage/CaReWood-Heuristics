
import java.io.IOException;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

public class Cluster extends Embedding {

	public Point clusterCenters[];
	public int centerPermutation[];
	boolean check[];
	public int NumberOfClusters;
	public GenPoint emClusterCenters[];

	
	public Cluster(){
		super();
	}
	
	public Cluster(int num){
		super();
		NumberOfClusters = num;
		
	}
	
	/*public void KmeansInputGivenPoints() {
		BufferedReader br = null;
		try {
			br = new BufferedReader(
					new InputStreamReader(new FileInputStream("C:\\Users\\palan\\workspace\\Kmeans\\clusterin.txt")));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String distance = "";
		int n1 = 0;
		try {
			String line;
			while ((line = br.readLine()) != null) {
				n1++;
				distance += line + " ";
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String a[] = distance.split(" ");
		n = n1;
		start = new Point[n];
		for (int i = 0; i < n; i++) {
			start[i] = new Point(1000 * Double.parseDouble(a[2 * i]), 1000 * Double.parseDouble(a[2 * i + 1]));
			// System.out.println(start[i].lat + " " + start[i].lon);
		}

	}*/

	public void KmeansInputRandom() {
		clusterCenters = new Point[NumberOfPoints];
		int stev = 0;
		for (int i = 0; i < NumberOfPoints; i++) {
			double ran = Math.random();
			// System.out.println(ran);
			if (ran < ((double) NumberOfClusters) / NumberOfPoints) {
				clusterCenters[stev] = map[i];
				stev++;
			}
			NumberOfClusters = stev;
		}
	}

	public void speckmeans() {
		int n1 = map.length;
		int integers[] = new int[n1];
		double probabilities[] = new double[n1];
		check = new boolean[n1];
		for (int i = 0; i < n1; i++) {
			integers[i] = i;
			probabilities[i] = capacities[i];
		}
		int num = NumberOfClusters;
		clusterCenters = new Point[NumberOfClusters];
		EnumeratedIntegerDistribution distr = new EnumeratedIntegerDistribution(integers, probabilities);
		while (num-- > 0) {
			int l = distr.sample();
			clusterCenters[num] = new Point(map[l].lat, map[l].lon);
			check[l] = true;
			for (int i = 0; i < n1; i++) {
				double s = Double.MAX_VALUE;
				for (int j = NumberOfClusters - 1; j >= num; j--) {
					double s1 = clusterCenters[j].diff(map[i]).norm();
					if (s1 < s) {
						s = s1;
					}

				}
				probabilities[i] = s * s * capacities[i];
				if (check[i])
					probabilities[i] = 0;
			}
			distr = new EnumeratedIntegerDistribution(integers, probabilities);

		}
		int ind = 0;
		centerPermutation = new int[NumberOfClusters];
		for(int i = 0; i<n1; i++){
			if(check[i]){
				centerPermutation[ind] = i;
				ind++;
			}
		}

	}
	
	public void specKmeansEmbedded(int numofClusters) throws IOException{
		NumberOfClusters = numofClusters;
		clusterCenters = new Point[NumberOfClusters];
		int integers[] = new int[NumberOfPoints];
		double probabilities[] = new double[NumberOfPoints];
		check = new boolean[NumberOfPoints];
		for (int i = 0; i < NumberOfPoints; i++) {
			//cap[i] = Double.parseDouble(a[4*i+3]);
			integers[i] = i;
			probabilities[i] = capacities[i];
		}
		//sort(all,0,n1-1);
		emClusterCenters = new GenPoint[NumberOfClusters];
		EnumeratedIntegerDistribution distr = new EnumeratedIntegerDistribution(integers, probabilities);
		int num = NumberOfClusters;
		while (num-- > 0) {
			int l = distr.sample();
			emClusterCenters[num] = new GenPoint(emPoints[l].coor);
			check[l] = true;
			for (int i = 0; i < NumberOfPoints; i++) {
				double s = Double.MAX_VALUE;
				for (int j = NumberOfClusters - 1; j >= num; j--) {
					double s1 = emClusterCenters[j].diff(emPoints[i]).norm();
					if (s1 < s) {
						s = s1;
					}

				}
				probabilities[i] = s * s * capacities[i];
				if (check[i])
					probabilities[i] = 0;
			}
			distr = new EnumeratedIntegerDistribution(integers, probabilities);

		}
		int ind = 0;
		centerPermutation = new int[NumberOfClusters];
		for(int i = 0; i<NumberOfPoints; i++){
			if(check[i]){
				centerPermutation[ind] = i;
				ind++;
			}
		}
	}
}


