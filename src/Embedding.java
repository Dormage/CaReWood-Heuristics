import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Embedding extends DataFilter{
	
	GenPoint emPoints[];

	public void Embed (){
		

		int m = distanceMatrix[0].length;
		double[][] distanceMatrix = new double[m][m];
		for(int i = 0; i < m; i++){
			for(int j = 0; j < m; j++){
				distanceMatrix[i][j] = Math.sqrt(this.distanceMatrix[i][j]);
			}
		}
		emPoints = new GenPoint[m];
		System.out.println(m);
		
		//PrintWriter writer = new PrintWriter("HistogramSLImprovedSQ.csv");
		int setSize = (int)Math.log((double)m);
	
		
		int set = (setSize+2);
		int dim = 4*set;
		//for(int dim = setSize; dim < setSize*setSize;dim++){
		double points[][] = new double[m][dim];
		ArrayList<Integer> checkSets[][] = new ArrayList[set][setSize];
		for(int i = 0; i < set; i++){
			for(int j = 0; j < setSize; j++){
			checkSets[i][j] = new ArrayList<Integer>();
			}
		}
		for(int i = 0; i < set; i++){
			double prob = 1;
			for(int j = 0; j < setSize; j++){
				prob = prob/2;
				for(int k = 0; k < m; k++){
					double pr = Math.random();
					if(prob>pr){
						checkSets[i][j].add(k);
					}
				}
				//System.out.println(checkSets[i][j].size());
			}
		}
		double min;
		for(int i = 0; i < m; i++){
			for(int j = 0; j < dim/set; j++){
				for(int j1 = 0; j1 < set; j1++ ){
					min = Double.MAX_VALUE;
					for(int k = 0; k < checkSets[j1][j].size(); k++){
						min = Math.min(min, distanceMatrix[i][checkSets[j1][j].get(k)]);
					}
					points[i][j*set+j1] = min;
				}
			}
		}
		double distorsion = 1.05;
		int l = 2*setSize;
		while(l-->=0){
			for(int i = 0; i < m; i++){
				for(int j = i+1; j < m; j++){
					double ratio = distance(points[i],points[j])/distanceMatrix[i][j];
					if(ratio>distorsion){
						shiftin(points[i],points[j],1-distorsion/ratio);
					}
					if(ratio<1/distorsion){
						shiftout(points[i],points[j],1/(ratio*distorsion));
					}
				}
			}
		}
		
	/*	double histogram[] = new double[11];
		double num = 0;
		double max = 0;
		double av = 0;
		distorsion  = 1.1;
		for(int i = 0; i < m; i++){
			for(int j = 0; j < m; j++){
				if((distance(points[i],points[j])/distanceMatrix[i][j]<distorsion && distance(points[i],points[j])/distanceMatrix[i][j]>1/distorsion) && distanceMatrix[i][j]!=0){
					num++;
				}
				if(distanceMatrix[i][j]!=0){
				max = Math.max(max,Math.max(distance(points[i],points[j])/distanceMatrix[i][j], distanceMatrix[i][j]/distance(points[i],points[j])));
				double fav= Math.max(distance(points[i],points[j])/distanceMatrix[i][j], distanceMatrix[i][j]/distance(points[i],points[j]));
				//System.out.println(fav);
				av += fav;
				}
				int ind = (int)(20*(Math.max(distance(points[i],points[j])/distanceMatrix[i][j], distanceMatrix[i][j]/distance(points[i],points[j]))-1));
				if(ind <= 10)
				histogram[ind]++;
				else histogram[10]++;
			}
		}
		System.out.println(num/(m*(m-1)) + " " + dim);
		System.out.println(av/(m*(m-1)));
		System.out.println(max);
		*/
		
	/*	for(int i = 0; i < histogram.length; i++){
			writer.print(histogram[i]/((double)(m*(m-1)))+",");
		}
		writer.println();
		writer.close();
		*/
		
		for(int i = 0; i < m; i++){
			emPoints[i] = new GenPoint(points[i]);
		}
	}
	
	
	private double distance(double A[],double B[]){
		double sum = 0;
		for(int i = 0; i < A.length;i++){
			sum+=(A[i]-B[i])*(A[i]-B[i]);
		}
		return Math.sqrt(sum);
	}
	
	private void shiftin(double A[],double B[],double coef){
		double C[] = new double[A.length];
		for(int i = 0; i < A.length; i++ ){
			C[i] = (A[i]+B[i])/2;
			A[i] = A[i] + coef*(C[i]-A[i]);
			B[i] = B[i] + coef*(C[i]-B[i]);
		}
	}
	private void shiftout(double A[],double B[],double coef){
		double C[] = new double[A.length];
		for(int i = 0; i < A.length; i++ ){
			C[i] = (A[i]+B[i])/2;
			A[i] = C[i] + coef*(A[i]-C[i]);
			B[i] = C[i] + coef*(B[i]-C[i]);
		}
	}
	
	
	
}
