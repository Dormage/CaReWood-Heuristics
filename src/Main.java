import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;

import lpsolve.LpSolveException;

public class Main {
	static NetworkManager manager;
	public static int numClustersLast =	1;
	public static double lastFitness = 0;
	static int numRepeats = 3;
	static int currentRepeatCount =	0;
	static long networkTime = 0;
	static long networkTimeDelta = 0;
	static long localTime = 0;
	static int sampleSize = 1000;
	static int [] sampleSizes = {100,200,300,400,500,600,700,800,900,1000};
	static int [] numberOfRepeats = {2,2,2,2,2,2,2,2,2,2};
	static boolean foundBestK=false;
	static int problemID =0;
	static int currentRepeat =0;
	static File dataSetFile = new File("Avstrija-podatki.json");
	static File distanceMatFile = new File("matrixFixed.json");
	static int runCount =0;
	static Kmeans k = new Kmeans(sampleSizes[0],dataSetFile,distanceMatFile,runCount);
	

//	public static double[] ResultTesting(int NumCenter, int b, int testSize) throws IOException, LpSolveException {
//		double statistics[] = new double[4];
//		double min = Double.MAX_VALUE;
//		double expectation = 0;
//		double stError = 0;
//		double overalTime = 0; 
//
//
//		Kmeans k = new Kmeans(sampleSize,dataSetFile,distanceMatFile,0);
//		//files written
//		for (int i = 0; i < testSize; i++) {
//			long t0 = System.currentTimeMillis();
//			k.StartEmbeded(NumCenter, b);
//			long t1 = System.currentTimeMillis();
//			overalTime += (double) (t1 - t0);
//			min = Math.min(k.TotalPrice, min);
//			expectation += k.TotalPrice / testSize;
//			stError += k.TotalPrice * k.TotalPrice;
//			// System.out.printf("%.3f\n", k.TotalPrice);
//		}
//		stError -= testSize * expectation * expectation;
//		stError /= testSize - 1;
//		stError = Math.sqrt(stError);
//		statistics[0] = min;
//		statistics[1] = expectation;
//		statistics[2] = stError;
//		statistics[3] = overalTime / testSize;
//		return statistics;
//	}

	
	public static void runTests(Double objectiveValue) throws IOException, LpSolveException{
		if(problemID == sampleSizes.length-1){
			System.out.println("First batch of tests completed, moving on to batch num: " + runCount);
			runCount++;
			currentRepeat=0;
			problemID=0;
			currentRepeatCount=0;
			numClustersLast=1;
			foundBestK=false;
			k = new Kmeans(sampleSizes[problemID],dataSetFile,distanceMatFile,runCount);
			k.StartEmbeded(1, 9999999);
			Parser parser = new Parser();
			String model = parser.euclidParseNetworked(k.map, k.clusterCenters, k.capacities, k.capPerCenter);
			manager.send(model);
			
		}
		if(currentRepeat == numberOfRepeats[problemID]){
			//move to next problem
			problemID++;
			k = new Kmeans(sampleSizes[problemID],dataSetFile,distanceMatFile,runCount);
			currentRepeat =0;
			currentRepeatCount=0;
			numClustersLast =1;
			System.out.println("Moving onto next problem size");
			computeModel(objectiveValue);	
		}else if(foundBestK){
			System.out.println("Max cluster reached!");
			currentRepeatCount=0;
			numClustersLast =1;
			currentRepeat++;
			foundBestK=false;
			computeModel(objectiveValue);
		}else{
			System.out.println("Re-run the previous problem and increase k ");
			computeModel(objectiveValue);
		}
		
	}
	
	public static void computeModel (Double objectiveValue) throws IOException, LpSolveException{
		long deltaNetwork = networkTimeDelta-networkTime;
		System.out.println("Received objective value for k= " + numClustersLast+ " : " + objectiveValue);
		System.out.println(numClustersLast + "," +objectiveValue+ "," +localTime+ "," +deltaNetwork);
		networkTime=0;
		localTime=0;
		networkTimeDelta =0;
		if(currentRepeatCount < numRepeats){
			long t0 = System.currentTimeMillis();
			currentRepeatCount++;
			k.StartEmbeded(numClustersLast, 9999999);
			Parser parser = new Parser();
			String model = parser.euclidParseNetworked(k.map, k.clusterCenters, k.capacities, k.capPerCenter);
			System.out.println("Repeating with same number of clusters: " + numClustersLast);
			localTime = System.currentTimeMillis() -t0;
			networkTime = System.currentTimeMillis();
			manager.send(model);
		}else if(lastFitness<objectiveValue){
			System.out.println("Was able to improve current best with " + numClustersLast + " clusters.");
			numClustersLast++;
			currentRepeatCount=0;
			lastFitness = objectiveValue;
			long t0 = System.currentTimeMillis();
			k.StartEmbeded(numClustersLast, 9999999); 
			Parser parser = new Parser();
			String model = parser.euclidParseNetworked(k.map, k.clusterCenters, k.capacities, k.capPerCenter);
			System.out.println("Increasing number of clusters to: " + numClustersLast);
			localTime = System.currentTimeMillis() -t0;
			networkTime = System.currentTimeMillis();
			manager.send(model);
		}else{
			System.out.println("Increasing the number of clusters to " + numClustersLast + " did not improve the objective value:");
			System.out.println("Best solution has " + (numClustersLast-1) + " of clusters with an objective value of: " + lastFitness);
			foundBestK = true;
			runTests(Double.MAX_VALUE);
		}
	}
	
	public static void main(String args[]) throws IOException, LpSolveException {
		String mode = "network";
		if (mode.equals("network")) {
			try {
				manager = new NetworkManager(new URI("ws://localhost:9003"));
			} catch (URISyntaxException e) {
				System.out.println("Error instanciating socket connection to server" );
				e.printStackTrace();
			}
			manager.connect();
		}
		long t0 = System.currentTimeMillis();
		k.StartEmbeded(1, 9999999);
		Parser parser = new Parser();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (mode.equals("network")) {
			// prepare the model as a string
			String model = parser.euclidParseNetworked(k.map, k.clusterCenters, k.capacities, k.capPerCenter);

			// send the fist model, this gets called only once. The rest of the
			// loop is handled by the callbacks
			localTime = System.currentTimeMillis() - t0;
			networkTime = System.currentTimeMillis();
			manager.send(model);
		} else {
			parser.euclidParse(k.map, k.clusterCenters, k.capacities, k.capPerCenter);
		}

	}
}
