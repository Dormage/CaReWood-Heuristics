import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import lpsolve.LpSolveException;

public class Kmeans extends Cluster{

	private static double e = 0.2;
	double pricePerCenter[];
	double capPerCenter[];
	double TotalPrice;
	double capBound;
	double maxi;
	private LinProgram solver;

	public Kmeans(int SampleSize, File dataSetFile, File distanceMatFile, int sampleIndex) {
		NumberOfPoints = SampleSize;
		int NumOfAll = 0;
		try {
			NumOfAll = ReadFiles(dataSetFile, distanceMatFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int indexArr[] = GenerateIndices(NumOfAll);
		try {
			JsonSampleGenerator(indexArr,sampleIndex);
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Embed();
		//super();
		//capBound = y;
		//centerPermutation = new int[x];
	/*	pricePerCenter = new double[NumberOfClusters];
		capPerCenter = new double[NumberOfClusters];
		solver = new LinProgram(NumberOfPoints, NumberOfClusters);
		solver.initialize(capacities, capBound);			*/							
		
	}

	public void startBound() throws IOException, LpSolveException {
		speckmeans();
		boolean run = true;
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		while (run) {

			double lats[] = new double[NumberOfClusters];
			double lons[] = new double[NumberOfClusters];
			for (int i = 0; i < NumberOfClusters; i++) {
				for (int j = 0; j < NumberOfPoints; j++) {
					alldistances[i * NumberOfPoints + j + 1] = map[j].diff(clusterCenters[i]).norm();
				}

			}
			double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
			solver.solve(alldistances, cmatrix);
			/*for(int i = 0; i < N; i++){
				for(int j = 0; j < in.m; j++){
					if(cmatrix[i][j] > 30){
						maxi = Math.max(maxi, map[j].diff(clusterCenters[i]).norm());
					}
				}
			}*/
			
			newStepBound(lats, lons, cmatrix);
			if (maxNorm(lats, lons, clusterCenters) < e) {
				run = false;
			}
			for (int j = 0; j < NumberOfClusters; j++) {
				clusterCenters[j].lat = lats[j];
				clusterCenters[j].lon = lons[j];
			}
		}
		//this.maxi = 2*maxi;
		shift();
		TotalPrice = CountCostBo();
	}

	public void startBoundReal() throws IOException, LpSolveException {
		boolean run = true;
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		while (run) {

			double lats[] = new double[NumberOfClusters];
			double lons[] = new double[NumberOfClusters];
			for (int i = 0; i < NumberOfClusters; i++) {
				for (int j = 0; j < NumberOfPoints; j++) {
					alldistances[i * NumberOfPoints + j + 1] = distanceMatrix[centerPermutation[i]][j];
				}

			}
			double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
			solver.solve(alldistances, cmatrix);
			newStepBound(lats, lons, cmatrix);
			if (maxNorm(lats, lons, clusterCenters) < e) {
				run = false;
			}
			for (int j = 0; j < NumberOfClusters; j++) {
				clusterCenters[j].lat = lats[j];
				clusterCenters[j].lon = lons[j];
			}
			shift();
		}
		TotalPrice = CountCostBoRe();
	}

	public void startUnBound() throws IOException, LpSolveException {
		boolean run = true;
		while (run) {
			double lats[] = new double[NumberOfClusters];
			double lons[] = new double[NumberOfClusters];
			double num[] = new double[NumberOfClusters];

			for (int i = 0; i < NumberOfPoints; i++) {
				double s = Double.MAX_VALUE;
				int index = 0;
				for (int j = 0; j < NumberOfClusters; j++) {
					double s1 = map[i].diff(clusterCenters[j]).norm();
					if (s1 < s) {
						s = s1;
						index = j;
					}

				}
				lats[index] += map[i].lat * capacities[i];
				lons[index] += map[i].lon * capacities[i];
				num[index] += capacities[i];

			}

			for (int j = 0; j < NumberOfClusters; j++) {
				lats[j] = lats[j] / num[j];
				lons[j] = lons[j] / num[j];
			}

			if (maxNorm(lats, lons, clusterCenters) < e) {
				run = false;
			}

			for (int j = 0; j < NumberOfClusters; j++) {
				clusterCenters[j].lat = lats[j];
				clusterCenters[j].lon = lons[j];
			}
		}
		shift();
		TotalPrice = CountCostBoRe();

	}

	private void shift() {
		int n = clusterCenters.length;
		int m = map.length;
		for (int j = 0; j < n; j++) {
			double s = Double.MAX_VALUE;
			int index = 0;
			for (int i = 0; i < m; i++) {
				double s1 = clusterCenters[j].diff(map[i]).norm();
				if (s1 < s) {
					s = s1;
					index = i;
				}
				maxi = Math.max(maxi, s);
			}
			clusterCenters[j].lat = map[index].lat;
			clusterCenters[j].lon = map[index].lon;
			centerPermutation[j] = index;
		}
	}
	private void genshift() {
		int n = emClusterCenters.length;
		int m = emPoints.length;
		for (int j = 0; j < n; j++) {
			double s = Double.MAX_VALUE;
			int index = 0;
			for (int i = 0; i < m; i++) {
				double s1 = emClusterCenters[j].diff(emPoints[i]).norm();
				if (s1 < s) {
					s = s1;
					index = i;
				}
			}
			for(int i = 0; i < emClusterCenters[0].dim; i++){
			emClusterCenters[j].coor[i] = emPoints[index].coor[i];
			centerPermutation[j] = index;
			}
		}
	}
	
	
	
	public void StartEmbeded(int clusterNum, double capBound) throws IOException, LpSolveException{
		
		this.capBound = capBound;
		specKmeansEmbedded(clusterNum);
		pricePerCenter = new double[NumberOfClusters];
		capPerCenter = new double[NumberOfClusters];
		solver = new LinProgram(NumberOfPoints, NumberOfClusters);
		solver.initialize(capacities, capBound);		
		
		
		boolean run = true;
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		
		while (run) {
			for (int i = 0; i < NumberOfClusters; i++) {
				for (int j = 0; j < NumberOfPoints; j++) {
					double dist = emPoints[j].diff(emClusterCenters[i]).norm();
					alldistances[i * NumberOfPoints + j + 1] = dist*dist;
				}
			}
			double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
			solver.solve(alldistances, cmatrix);
			GenPoint[] newPoints = newStepEmbedded(cmatrix);
			double epsilon = genMaxNorm(newPoints, emClusterCenters);
			if (epsilon < e) {
				run = false;
			}
			for (int j = 0; j < NumberOfClusters; j++) {
				for(int i = 0; i < emClusterCenters[0].dim; i++){
				emClusterCenters[j].coor[i] = newPoints[j].coor[i];
				}
			}
		}
		genshift();
		for(int i = 0; i< NumberOfClusters; i++){
			clusterCenters[i] = map[centerPermutation[i]];
		}
		
		TotalPrice = countCostEm();
		
		
	}

	private void newStepBound(double lats[], double lons[], double[][] cmatrix) {
		int n = lats.length;
		int m = cmatrix[0].length;
		for (int i = 0; i < n; i++) {
			double uplat = 0;
			double uplon = 0;
			double downlat = 0;
			double downlon = 0;
			for (int j = 0; j < m; j++) {
				uplat += cmatrix[i][j] * map[j].lat;
				uplon += cmatrix[i][j] * map[j].lon;
				downlat += cmatrix[i][j];
				downlon += cmatrix[i][j];
			}
			lats[i] = uplat / downlat;
			lons[i] = uplon / downlon;
		}
	}
	
	private GenPoint[] newStepEmbedded( double[][] cmatrix){
		GenPoint[] newPoints = new GenPoint[cmatrix.length];
		for(int i = 0; i < emClusterCenters.length; i++){
			newPoints[i] = weightedMean(emPoints,cmatrix[i]);
		}
		return newPoints;
	}

	private GenPoint weightedMean(GenPoint points[], double weights[]){
		double num = points.length;
		int dim = points[0].dim;
		double a[] = new double[dim];
		double brojilac;
		double imenilac;
		for(int i = 0; i < dim; i++){
			brojilac = 0;
			imenilac = 0;
			for(int j = 0; j < num; j++){
				brojilac+=weights[j]*points[j].coor[i];
				imenilac+=weights[j];
			}
			a[i] = brojilac/imenilac;
		}
		GenPoint m = new GenPoint(a);
		return m;
		
		
	}
	
/*	double CountCostUN(Point[] in, Point[] cent, double[] pricePerCenter, double capacities[], double capPerCenter[]) {
		int m = in.length;
		int n = cent.length;
		for (int i = 0; i < m; i++) {
			double s = Double.MAX_VALUE;
			int index = 0;
			for (int j = 0; j < n; j++) {
				double s1 = in[i].diff(cent[j]).norm();
				if (s1 < s) {
					s = s1;
					index = j;
				}
			}
			pricePerCenter[index] += capacities[i] * in[i].diff(cent[index]).norm();
			capPerCenter[index] += capacities[i];
		}
		double sum = 0;

		for (int i = 0; i < n; i++) {
			sum += pricePerCenter[i];
		}
		return sum;
	}

	double CountCostBo(Point[] in, Point[] cent, double[] pricePerCenter, double capacities[], double capBound,
			double capPerCenter[]) throws IOException, LpSolveException {
		int n = cent.length;
		int m = in.length;
		double alldistances[] = new double[n * m + 1];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				alldistances[i * m + j + 1] = in[j].diff(cent[i]).norm();
			}

		}
		double cmatrix[][] = new double[N][m];
		solver.solve(alldistances, cmatrix);
		int sum = 0;
		for (int i = 0; i < n; i++) {
			pricePerCenter[i] = 0;
			capPerCenter[i] = 0;
			for (int j = 0; j < m; j++) {
				pricePerCenter[i] += cmatrix[i][j] * in[j].diff(cent[i]).norm();
				capPerCenter[i] += cmatrix[i][j];
			}
			sum += pricePerCenter[i];
		}
		return sum;

	}
*/
	// TotalPrice = CountCostBoRe(map, centerPermutation, pricePerCenter, capacities, capBound, capPerCenter);
	
	
	private double CountCostBoRe() throws IOException, LpSolveException {
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		for (int i = 0; i < NumberOfClusters; i++) {
			for (int j = 0; j < NumberOfPoints; j++) {
				alldistances[i * NumberOfPoints + j + 1] = this.distanceMatrix[centerPermutation[i]][j];
				
			}
		}
		
		double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
		solver.solve(alldistances, cmatrix);
		int sum = 0;
		for (int i = 0; i < NumberOfClusters; i++) {
			pricePerCenter[i] = 0;
			capPerCenter[i] = 0;
			for (int j = 0; j < NumberOfPoints; j++) {
				pricePerCenter[i] += cmatrix[i][j] * this.distanceMatrix[centerPermutation[i]][j] / 1000;
				capPerCenter[i] += cmatrix[i][j];
			}
			sum += pricePerCenter[i];
		}
		return sum;

	}
	
	
	private double CountCostBo() throws IOException, LpSolveException {
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		for (int i = 0; i < NumberOfClusters; i++) {
			for (int j = 0; j < NumberOfPoints; j++) {
				alldistances[i * NumberOfPoints + j + 1] = map[j].diff(clusterCenters[i]).norm();
				
			}
		}
		
		double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
		solver.solve(alldistances, cmatrix);
		int sum = 0;
		for (int i = 0; i < NumberOfClusters; i++) {
			pricePerCenter[i] = 0;
			capPerCenter[i] = 0;
			for (int j = 0; j < NumberOfPoints; j++) {
				pricePerCenter[i] += cmatrix[i][j] * map[j].diff(clusterCenters[i]).norm() / 1000;
				capPerCenter[i] += cmatrix[i][j];
			}
			sum += pricePerCenter[i];
		}
		return sum;

	}
	
	

	private double maxNorm(double lats[], double lons[], Point[] clusterCenters) {
		int n = lats.length;
		double maximal = 0;
		for (int j = 0; j < n; j++) {
			Point k = new Point(lats[j], lons[j]);
			maximal = Math.max(maximal, clusterCenters[j].diff(k).norm());
		}
		return maximal;

	}
	
	private double genMaxNorm(GenPoint oldclusterCenters[], GenPoint clusterCenters[] ){
		int n = clusterCenters.length;
		double maximal = 0;
		double norm = 0;
		for (int j = 0; j < n; j++) {
			norm = clusterCenters[j].diff(oldclusterCenters[j]).norm();
			maximal = Math.max(maximal, norm);
		}
		return maximal;
		
	}
	
	double countCostEm() throws IOException, LpSolveException{
		double alldistances[] = new double[NumberOfClusters * NumberOfPoints + 1];
		for (int i = 0; i < NumberOfClusters; i++) {
			for (int j = 0; j < NumberOfPoints; j++) {
				alldistances[i * NumberOfPoints + j + 1] = this.distanceMatrix[centerPermutation[i]][j];
			}
			
		}
		double cmatrix[][] = new double[NumberOfClusters][NumberOfPoints];
		solver.solve(alldistances, cmatrix);
		int sum = 0;
		for (int i = 0; i < NumberOfClusters; i++) {
			pricePerCenter[i] = 0;
			capPerCenter[i] = 0;
			for (int j = 0; j < NumberOfPoints; j++) {
				pricePerCenter[i] += cmatrix[i][j] * this.distanceMatrix[centerPermutation[i]][j] / 1000;
				capPerCenter[i] += cmatrix[i][j];
			}
			sum += pricePerCenter[i];
			capPerCenter[i]= 100000;
		}
		return sum;
	}
	

}
